# Port Stanley 

[[_TOC_]]

This is my remake of the wargame _Port Stanley_.   The original game
was published by World Wide Wargames (3W) in 1984 (two years after the
conflict). 

## Game mechanics 

|              |                   |
|--------------|-------------------|
| Period       | Modern            |
| Level        | Operational       |
| Hex scale    | 2.8km (1.7 miles) |
| Unit scale   | company (II)      |
| Turn scale   | 2 days            |
| Unit density | Low               |
| # of turns   | 15                |
| Complexity   | 7 of 10           |
| Solitaire    | 3 of 10           |

Features:
- Land war 
- Air war
- Naval war
- Combined arms 
- Fog-of-war

## About this rework

This work is entirely new.  All text and graphics are new and nothing
is copied verbatim from the original materials.  

I have restructured and rewritten the rules, which hopefully will make
the game more accessible.  I also added some illustrations to this
purpose. 

## The board and counters

The board is quite big (70.2cm tall and 56cm wide), and does not fit on
regular paper formats.  The board is therefore available in
section.   There is one document designed to be printed on an A4
printer, and one designed to be printed on an A3 printer. 

- [splitboardA3.pdf][] has 4 sheets of A3 paper. 
- [splitboardA4.pdf][] has 8 sheets of A4 paper. 

For Letter and Tabloid, substitute names appropriately.  The letter
and tabloid versions are scaled to 94% of the A4 and A3 version,
including the counters, so as to fit on those paper formats. 

The dimensions of the components are 

| *Component* | *A4-series*   | *Letter-series* |
|-------------|---------------|-----------------|
| Board       | 70.2cm x 56cm | 25.8" x 20.7"   |
| Counters    | 1.1cm x 1.1cm | 0.4" x 0.4"     |

and the set of files are 

| *Component* | *A4-series*          | *Letter-series*           |
|-------------|----------------------|---------------------------|
|             | [splitboardA3.pdf][] | [splitboardTabloid.pdf][] |
| Board       | [splitboardA4.pdf][] | [splitboardLetter.pdf][]  |
| Counters    | [materialsA4.pdf][]  | [materialsLetter.pdf][]   |
| Tables      | [tablesA4.pdf][]     | [tablesLetter.pdf][]      |


Print the document of choice, and glue onto a sturdy surface, e.g.,
1.5mm (0.058") poster board.  The sheets overlap, so care should be
taken to cut out the images at the crop marks.  Alternatively, glue
the sheets together for a paper board.

## The files 

The distribution consists of the following files 

- [PortStanleyA4.pdf][] This is a single document that contains
  everything: The rules, charts, counters, and order of battle, and
  the board split into 10 parts.
  
  This document is meant to be printed on a duplex A4 printer.  The
  charts, OOB charts, counters, and board will be on separate sheets
  and can be separated from the rules.
  
  Note that the PDF is a little heavy to load (due to the large map),
  so a bit of patience is needed. 
  
- [PortStanleyA4Booklet.pdf][] This document _only_ contains the
  rules.  It is meant to be printed on a duplex A4 printer with long
  edge binding.  This will produce 6 sheets which should be folded
  down the middle of the long edge, and stabled to form an 24-page A5
  booklet of the rules.

- [tablesA4.pdf][] This document has all tables collect, for easy
  reference. 
  
- [tablesA4Booklet.pdf][] As above, but readied to be made into an 8
  page booklet. 
  
- [splitboardA3.pdf][] holds the board in 5 sheets of A3 paper.  Print
  these and glue on to a sturdy piece of cardboard.  You can perhaps
  make some clever arrangement that allows you to unfold the map.
  Otherwise, use paper clambs or the like to hold the board pieces
  together.  
  
- [splitboardA4.pdf][] is like [splitboardA3.pdf][] above, but further
  split to fit on 10 sheets of A4 paper.   
  
- [materialsA4.pdf][] holds the charts, counters, and OOB.  It is
  meant to be printed on A4 paper.  Print and glue on to   a
  relatively thick piece of cardboard (1.5mm or so) or the like. 

- [board.pdf][] is the entire board in one PDF.  If you have a way of
  printing such a large sheet, perhaps this can be of use to you. 
  

If you only have access to US Letter (and possibly Tabloid) printer,
you can use the following files

| *A4 Series*                  | *Letter Series*                     |
| -----------------------------|-------------------------------------|
| [PortStanleyA4.pdf][]        | [PortStanleyLetter.pdf][]  	     |
| [PortStanleyA4Booklet.pdf][] | [PortStanleyLetterBooklet.pdf][]    |
| [materialsA4.pdf][]		   | [materialsLetter.pdf][]			 |
| [tablesA4.pdf][]		       | [tablesLetter.pdf][]			     |
| [tablesA4Booklet.pdf][]	   | [tablesLetterBooklet.pdf][]		 |
| [splitboardA4.pdf][]		   | [splitboardLetter.pdf][]			 |
| [splitboardA3.pdf][]		   | [splitboardTabloid.pdf][]			 |

Note, the US letter files are scaled down to 95% relative to the A4
files.  This is to fit everything in on the same scale.  This should
not be a problem for most users as most of the world is using A4
rather than the rather obscure Letter format. 

Download [artifacts.zip][] to get all files for both kinds of paper
formats. 
	
## VASSAL Module 

There is a [VASSAL](https://vassalengine.org) module available 

- [PortStanley.vmod][] 

This is made from the same sources as the Print'n'Play documents. 

## Source files 

- [`ps.sty`](ps.sty) Package containing definitions used by other
  sources 
- [`PortStanley.tex`](PortStanley.tex) Driver for the main document 
- [`front.tex`](front.tex) Front page and colophon
- [`preface.tex`](preface.tex) Preface and table of contents 
- [`rules.tex`](rules.tex) The rules part 
- [`materials.tex`](materials.tex) Driver of materials document 
- [`units.tex`](units.tex) Definition of counters 
- [`tables.tex`](tables.tex) Tables, charts, and OOBs 
- [`board.tex`](board.tex) The board 
- [`calcsplit.tex`](calcsplit.tex) Calculate split of board over
  sheets 
- [`spine.tex`](spine.tex) Spine on box 
- [`box.tex`](box.tex) Counter box covers 

## Previews 

[![The board](.imgs/board.png)](https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-A4-master/board.pdf?job=dist "The board")
[![The rules](.imgs/frontA4.png)](https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-A4-master/PortStanleyA4.pdf?job=dist)

[![The rules](.imgs/units.png)](https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-A4-master/materialsA4.pdf?job=dist)

[![The VASSAL module](.imgs/vassal.png)](https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-A4-master/PortStanley.vmod?job=dist)

## Implementation 

The whole package is implemented in LaTeX using my package
[_wargame_](https://gitlab.com/wargames_tex/wargame_tex).  This package,
combined with the power of LaTeX, produces high-quality documents,
with vector graphics to ensure that everything scales.   Since the
board is quite big, we must use Lua$`\mathrm{\LaTeX}`$ for the
formatting. 

## Copyright and license

(c) 2022 Christian Holm Christensen 

This work is licensed under the Creative Commons
Attribution-ShareAlike 4.0 International License (CC-BY-SA-4.0). To
view a copy of this license, visit [CC][11] or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

## The conflict 

The conflict started on 1st of April, 1982, when Argentina invaded the
Falkland Islands, and ended on 14th of June the same year with the
British captured Stanley.   However, the origins of the conflict date
back hundreds of year, depending on the point of view. 

Essentially, the Argentine claim to the Falklands, or as they call it
_Islas Melvinas_, is that when Argentina gain independence from Spain,
it inherited the Falklands.   The British claim is that the
inhabitants of the islands wish to be part of the United Kingdom. 

The inhabitants of Falklands Islands has since the end of the war in
1982 continuously asserted they affiliation to the United Kingdom,
latest in a 2013 referendum
([Wikipedia](https://en.wikipedia.org/wiki/Falkland_Islands\#Sovereignty_dispute)
and [Falkland Islands government web-page](https://www.falklands.gov.fk/our-home)) where an
overwhelming majority voted to stay in the United Kingdom.  The
Falkland Islands are sometimes referred to as _Islas Melvinas_, which
the inhabitants of the islands however consider
[offensive](https://www.falklands.gov.fk/visitors/visiting-the-falkland-islands\#collapse275).
This documents therefore refrain from using that term.

The war, or perhaps rather _conflict_ since neither party officially
declared war, has been called 
["... a fight between two bald men over a comb"](https://www.theguardian.com/commentisfree/2010/feb/19/falkland-islands-editorial),
and a often spoken view is that British victory was inevitable.
However, as this game shows, the victory of the British was not so
straight forward.  The logistical challenge of fighting almost 7,000
nautical miles (more than 12,000 kilometres or 7,500 miles) away from
home is huge, and coupled with the gruelling weather of the Antarctic
early winter, massively outnumbered British, and lack of air
superiority, the fight was all but a given.

As such, the game is a study of modern warfare, including combined
arms and logistics, and much can probably be learned relevant to other
modern conflicts (e.g., the Russian invasion of Ukraine in February
2022 where the Russians also faced logistical challenges and did not
establish air supremacy either).


[artifacts.zip]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/download?job=dist

[PortStanleyA4.pdf]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-A4-master/PortStanleyA4.pdf?job=dist
[PortStanleyA4Booklet.pdf]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-A4-master/PortStanleyA4Booklet.pdf?job=dist
[materialsA4.pdf]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-A4-master/materialsA4.pdf?job=dist
[tablesA4.pdf]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-A4-master/tablesA4.pdf?job=dist
[tablesA4Booklet.pdf]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-A4-master/tablesA4Booklet.pdf?job=dist
[splitboardA3.pdf]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-A4-master/splitboardA3.pdf?job=dist
[splitboardA4.pdf]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-A4-master/splitboardA4.pdf?job=dist
[board.pdf]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-A4-master/board.pdf?job=dist
[PortStanley.vmod]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-A4-master/PortStanley.vmod?job=dist


[PortStanleyLetter.pdf]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-Letter-master/PortStanleyLetter.pdf?job=dist
[PortStanleyLetterBooklet.pdf]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-Letter-master/PortStanleyLetterBooklet.pdf?job=dist
[materialsLetter.pdf]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-Letter-master/materialsLetter.pdf?job=dist
[tablesLetter.pdf]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-Letter-master/tablesLetter.pdf?job=dist
[tablesLetterBooklet.pdf]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-Letter-master/tablesLetterBooklet.pdf?job=dist
[splitboardTabloid.pdf]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-Letter-master/splitboardTabloid.pdf?job=dist
[splitboardLetter.pdf]: https://gitlab.com/wargames_tex/portstanley_tex/-/jobs/artifacts/master/file/PortStanley-Letter-master/splitboardLetter.pdf?job=dist
