from wgexport import *
#
# TBD
#
# Select weather via specific Sendto
#
# Select beaches via Sendto (middle slot) and flip
#
# Command to remove the remaining?  Should be masked?  No.  AG must be
# able to inspect.
#
moreHelp = '''
<html>
 <head>
  <title>{title}</title>
 </head>
 <body>
  <h1>{title}</h1>
  <h2>Hidden Argentine units</h2>
  <p>
   At the start of a game all Argentine units are hidden.
   Their state can be toggle freely by the Argentine faction at
   this point.
  </p>
  <p>
   After the initial setup, Argentine units can only be
   revealed.  That is, they cannot be hidden again.
  </p>
  <h2>Weather determination</h2>
  <p>
   The factions can <i>either</i>
  </p>
  <ul>
   <li>Right click the weather markers and select &quot;Resolve&quot;,
    which will roll a die and resolve according to weather table,
    <i>or</i></li>
   <li>Roll the die (Alt-6), consult the weather chart, and use
    right-click menu entried to move the weather markers to the
    appropriate slot.</li>
  </ul>
  <p>
   The primary weather <i>must</i> be resolved <i>first</i> in both
   cases.  Note, the weather is always <i>Clear</i> on turn 1, so only
   the secondary weather need to be resolved. 
  </p>
  <h2>British landing zones</h2>
  <p>
   The British faction must choose 3 landing zones from its OOB
   and place them on the map.  Use the right-context menu and
   select &quot;Select&quot; to send a beach marker to the board 
   (they all go in the same slot, but the faction can correct that).
   The beach markers are automatically flipped to the reverse side so
   that the Argentine faction cannot see them. 
  </p>
  <p>
   Once the three landing zones have been selected, then the rest of the
   beach markers must be deleted from the British OOB, least the
   Argentine faction should deduce the possible landing zones.  To
   that purpose, select the remaining markers, and use right-click
   context menu and select &quot;Clean&quot;
  </p>
  <h2>Argentine morale points</h2>
  <p>
   Two buttons on top of the interace allows the
   factions to increment and decrement the number
   of Argentine morale points. Morale points can also be
   incremented via the right-click context menu or via the
   short cuts <code>+</code> and <code>-</code> if the marker
   is selected. 
  </p>
  <p>
   If the morale points fall to 0, then a pop-up is displayed to alert
   the factions that the game is over and victory points should be
   tallied.
  </p>
  <h2>Turns and phases</h2>
  <p>
   Use the turn and phase interface at the top of the main window.
   Judicial use of this allows for certain actions to be partially
   automated and restrict certain actions to appropriate times.
  </p>
  <h2>Air factors</h2>
  <p>
   Air factors should only ever be decreased, except
   if the British faction reallocates Harriers to the
   off-map pool (Task force carriers) or an optional rule
   is used.  AFs can be descreased by pressing <code>-</code>
   when the marker is selected, or via the right click context
   menu.
  </p>
  <h2>Eliminated units</h2>
  <p>
   Regular units are send to the dead pool (trash can icon).
   Faction support units (improved positions, airfields, and
   so on) are sent back to the OOB.
  </p>
  <h2>Historic setup</h2>
  <p>
   By pressing the Argentine or Union Jack button on top,
   the Argentine and British faction forces, respectively,
   are setup more or less historically.
  </p>
  <h2>About this game</h2>
  <p>
   This VASSAL module was created from
   L<sup>A</sup>T<sub>E</sub>X sources of a Print'n'Play version
   of the <i>Port Stanley</i> game.  That (a PDF) can be
   found at
  </p>
  <center>
   <code>https://gitlab.com/wargames_tex/portstanley_tex</code></a>
  </center>
  <p>
   where this module, and the sources, can also be found.  The PDF
   can also be found as the rules of this game, available in the
   <b>Help</b> menu.
  </p>
  <p>
   The original game was release by the World Wide Wargaming.
  <h3>Credits</h3>
  <dl>
   <dt>Game Design</dt>
   <dd>Albert C.E. Parker</dd>
   <dt>Game Development</dt>
   <dd>Roger G. Nord, Jim Hind, Kieth Poulter, &amp; Wallace Poulter</dd>
   <dt>Original Graphics Concepts:</dt>
   <dd>Thomas D. Ciampa</dd>
   <dt>Cover Art</dt>
   <dd>Jan French</dd>
   <dt>Map</dt>
   <dd>Ina Clausen</dd>
   <dt>Counters</dt>
   <dd>Howard Bond</dd>
   <dt>Playtesting</dt>
   <dd>Benjamin Butterfield VII,
    Mark A. Campbell,
    Thomas D. Ciampa,
    Mark B. Cohen,
    Marcus Ferro,
    Stewart Johnson,
    Dennis Largess,
    Mark McLaughlin,
    Larry Marotti,
    James Minnow,
    Kevin Nealon,
    George T. Parker,
    Donald E. William,
    and associates.
   </dd>
   <dt>Publisher</dt>
   <dd>World Wide Wargames</dd>
  </dl>
  <h2>Copyright and license</h2>
  <p>
   This work is &#127279; 2022 Christian Holm Christensen, and
   licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit
  </p>
  <center>
   <code>http://creativecommons.org/licenses/by-sa/4.0</code>
  </center>
  <p>
   or send a letter to
  </p>
  <center>
   Creative Commons<br>
   PO Box 1866<br>
   Mountain View<br>
   CA 94042<br>
   USA
  </center>
 </body>
</html>
'''

# --------------------------------------------------------------------
startHexes = {
    "MP 12": ["morale"],
    "ag offmap airfield": ["ag airfield 3",
                           "ag 3 12 3 3 1 aiplt",
                           "ag pucara 4 2"],
    "3613": ["ag airfield 2", "ag pucara 2 2"],
    "0925": ["ag 3 12 1 aibtn",
             "ag 3 12 2 1 aicoy",
             "ag 3 12 3 1 aicoy",
             "ag improved 8"],    
    "0926": ["ag airfield 1",
             "ag pucara 2 1",#Flipped
             "ag 3 1 arcoy",
             "ag 601 1 adcoy",
             "ag improved 9"],
    "0924": ["uk 22sas dg sfplt"],
    "0812": ["ag 3 12 3 2 aicoy"],
    "0810": ["ag 3 12 3 3 2 aiplt"],
    "0611": ["uk lancelot geraint",
             "uk 3 3 ab abibtn"],
    "0612": ["uk galahad tristram",
             "uk supply 2 1",#Flipped
             "uk 3 12 t 1 adplt"],
    "0610": ["uk glamorgan",
             "uk brilliant coventry"],
    "0614": ["uk plymouth yarmouth",
             "uk fearless",
             "uk 3 40 ab maribtn",
             "uk 3 29 1 arbat"],
    "0615": ["uk arrow antelope",
             "uk intrepid",
             "uk 3 2 ab abibtn",
             "uk 3 29 1 arbat"],
    "0713": ["uk bedivere precivale",
             "uk 3 45 xy maribtn"],
    "3721": ["ag 3 4 2 aibtn",
             "ag 3 4 3 aibtn",
             "ag improved 2"],
    "3818": ["ag 3 4 1 aibtn",
             "ag decoy 2",
             "ag improved 3"],    
    "4016": ["ag 10 7 3 mibtn",
             "ag 10 7 2 mibtn",
             "ag improved 4"],
    "4217": ["ag 10 7 1 mibtn",
             "ag decoy 3",
             "ag improved 5"],    
    "4118": ["ag 3 2 arcoy",
             "ag 3 3 arcoy"],
    "4019": ["ag 10 6 3 mibtn",
             "ag 10 6 2 mibtn",
             "ag improved 6"],    
    "4120": ["ag 10 6 1 mibtn",
             "ag improved 7"],
    "4119": ["ag 1m 1 arcoy",
             "ag 1m 2 arcoy"],    
    "4419": ["ag jf hq",
             "ag 10 3 3 mibtn",
             "ag 10 3 2 mibtn"],
    "4319": ["ag 10 hq",
             "ag decoy 4"],
    "4420": ["ag 10 arecbtn",
             "ag decoy 6"],    
    "4618": ["ag helicopter a",
             "ag helicopter b",
             "ag pucara 4 1",
             "ag 10 3 1 mibtn",
             "ag 601 2 adcoy",
             "ag m 1 adcoy",
             "ag m 2 adcoy",
             "ag improved 10"],
    "4220": ["ag 25 3 ibtn",
             "ag 25 2 ibtn"],
    "4219": ["ag 25 1 ibtn",
             "ag decoy 5"],    
    "4520": ["ag exocet",
             "ag apc a",
             "ag apc b",
             "ag 1ab 1 1 abicoy"],
    "4018": ["ag m 3 maribtn",
             "ag m 5 1 maricoy"],    
    "4320": ["ag 11 aarcoy",
             "ag r 601 icoy",
             "ag r 602 icoy"],    
    "3418": ["ag 3 12 2 2 aicoy",
             "ag decoy 1",
             "ag improved 1"]
}
revHexes = {v: k for k,vs in startHexes.items() for v in vs }

# --------------------------------------------------------------------
def makeTurnPrototypes(main,
                       turns,
                       phaseNames,
                       prototypeContainer):
    with VerboseGuard('Prototypes for turns') as v:
        turnPrototypes     = []
        for t in range(1,16):
            if t == 0: v(' ',end='')
            v(f'[{t}] ',end='')
            k  = key(NONE,0)+f',Turn{t}'
            turns.addHotkey(hotkey       = k,
                            match        = (f'{{Turn=={t}&&'
                                            f'Phase=="{phaseNames[0]}"}}'),
                            reportFormat = f'<b>=== Turn {t} ===</b>',
                            name         = f'Turn {t}')
            main.addMassKey(name         = f'Turn marker to {t}',
                            buttonHotkey = k,
                            hotkey       = k,
                            buttonText   = '', 
                            target       = '',
                            filter       = ('{BasicName == "game turn"}'))
            
            pn     = f'To turn {t}'
            traits = [ SendtoTrait(mapName     = 'Board',
                                   boardName   = 'Board',
                                   name        = '',
                                   restoreName = '',
                                   restoreKey  = '',
                                   zone        = f'Turns',
                                   destination = 'R', #R for region
                                   region      = ('game turn' if t == 1 else
                                                  f'turn {t}'),
                                   key         = k,
                                   x           = 0,
                                   y           = 0),
                       BasicTrait()]
            prototypeContainer.addPrototype(name        = f'{pn} prototype',
                                            description = f'{pn} prototype',
                                            traits      = traits)
            turnPrototypes.append(pn)
        v('')

        return turnPrototypes
    

# --------------------------------------------------------------------
def makeMPPrototypes(incrMPKey,
                     decrMPKey,
                     incrAFKey,
                     decrAFKey,
                     prototypeContainer):
    with VerboseGuard('Prototypes for MPs') as v:
        expr  = 'Integer.parseInt(LocationName.replaceAll("[^0-9]",""))'
        val   = CalculatedTrait(name        = 'MPCounter',
                                expression  = expr,
                                description = 'Calculate current value')
        iCode = f'{{"MP "+Math.min(MPCounter+1,17)}}'
        dCode = f'{{"MP "+Math.max(MPCounter-1,0)}}'
        incr  = SendtoTrait(mapName     = f'Board',
                            boardName   = f'Board',
                            restoreName = '',
                            restoreKey  = '',
                            name        = '', # 'Increment',
                            destination = 'R', #R for region
                            region      = iCode,
                            key         = incrMPKey)
        decr  = SendtoTrait(mapName     = f'Board',
                            boardName   = f'Board',
                            restoreName = '',
                            restoreKey  = '',
                            name        = '', # 'Decrement',
                            destination = 'R', #R for region
                            region      = dCode,
                            key         = decrMPKey)
        aincr  = TriggerTrait(name     = 'Increment MP (indirectly)',
                              command  = 'Increment',
                              key      = incrAFKey,
                              actionKeys = [incrMPKey])
        adecr  = TriggerTrait(name     = 'Decrement MP (indirectly)',
                              command  = 'Decrement',
                              key      = decrAFKey,
                              actionKeys = [decrMPKey])
        rt     = ('{MPCounter<=0 ? '
                  'Alert("Argenine faction surrenders. Tally VPs") : '
                  '"? Argentine MPs: "+MPCounter}')
        rep    = ReportTrait(incrMPKey,decrMPKey,  report = rt)
        lim    = RestrictAccessTrait(sides=[],description='Cannot move')
        traits = [val,aincr,adecr,incr,decr,rep,lim,BasicTrait()]
        prototypeContainer.addPrototype(name        = 'Argentine MP',
                                        description = 'Argentine MP',
                                        traits      = traits)


# --------------------------------------------------------------------
def weatherTraits(name,
                  traits,
                  main,
                  phaseNames,
                  primaryWeather,
                  secondaryWeather,
                  rollDice,
                  calcWeather,
                  resolveWeather):
    #
    # This is a little tricky.  The weather pieces
    # defines the traits
    #
    # - GlobalHotkeyTrait that rolls the dice
    # - CalculatedPropertyTrait that stores value
    #   of modified die roll
    # - CalculatedPropertyTrait that read current
    #   location and stored the weather there.
    # - SendtoTrait that, based
    #   - The stored die roll
    #   - The current weather
    #   - A hard-coded table
    #   deduces where to send the piece
    # - A trigger action that first fires the die roll
    #   and then the send to trait
    #
    with VerboseGuard('Adding weather resolution') as v:
        weatherTargets = {
            'weather': {
                'targets': ['Clear', 'Cloudy', 'Stormy' ],
                'var':     'Weather1',
                'global':  primaryWeather,
                'table':   [['Clear',  [0,0,1,1,2,2,2]],
                            ['Cloudy', [0,0,1,1,1,2,2]],
                            ['Stormy', [0,0,0,1,1,2,2]]]
            },
            'sub weather': {
                'targets': ['None',
                            'Cold',
                            'Calm',
                            'Foggy',
                            'Rough'],
                'var':     'Weather2',
                'global':  secondaryWeather,
                'table':   [['Clear',  [1,0,0,0,0,2]],
                            ['Cloudy', [1,1,0,3,4,2]],
                            ['Stormy', [1,1,1,3,4,4]]]
            }
        }
        
        targets = weatherTargets[name]['targets']
        var     = weatherTargets[name]['var']
        vn      = 'Primary' if var == 'Weather1' else 'Secondary'
        prv     = primaryWeather
        gv      = weatherTargets[name]['global']
        table   = weatherTargets[name]['table']
        start   = f'weather {targets[0]}'
        atStart = main.addAtStart(name        = name,
                                  location    = start,
                                  owningBoard = 'Board')
        expr    = (
            '{"weather "+('
            +(':'.join([(f'{prv}=="{t}"?('+
                         ':'.join([f'Die=={n+1}?"{targets[r]}"'
                               for n,r in enumerate(row)])
                         +f':"{targets[row[0]]}")')
                    for t,row in table])
              +f':"{targets[0]}"')+')}')
        grp      = (f'{{"{prv}="+{prv}+" modified die roll="+Die}}')
        
        trgs = []                    
        traits.extend([
            CalculatedTrait(
                name = var,
                expression = 'LocationName.replaceAll("weather ","")',
                description = 'Calculate current value'),
            CalculatedTrait(
                name = 'Die',
                expression = ('GetProperty("1d6_result")'
                              +('+(Turn>6 ? 1 : 0)' if
                                name=='weather' else '')),
                description = 'Calculate modified die roll'),
            SendtoTrait(mapName     = f'Board',
                        boardName   = f'Board',
                        restoreName = '',
                        restoreKey  = '',
                        name        = '',
                        destination = 'R', #R for region
                        region      = expr,
                        key         = resolveWeather),
            GlobalHotkeyTrait(name         = '',
                              key          = rollDice,
                              globalHotkey = key('6',ALT),
                              description  = 'Roll dice'),
            GlobalPropertyTrait(
                ['',calcWeather,GlobalPropertyTrait.DIRECT,
                 #{{LocationName.replaceAll("weather ","")}}'
                 f'{{{var}}}'
                 ],
                name = gv,
                level= GlobalPropertyTrait.NAMED_MAP,
                search = 'Board'),
            #ReportTrait(calcWeather,report=grp),
        ])
        trgs.append(
            TriggerTrait(name       = f'Resolve {var}',
                         command    = 'Resolve',
                         key        = key(NONE,0)+',resolveIt',
                         actionKeys = [rollDice,
                                       calcWeather,
                                       resolveWeather,
                                       calcWeather]))
        
        lk = [resolveWeather]
        for target in targets:
            v(f'{name} -> {target}')
            ky = key(NONE,0)+f',{target}'
            trgs.append(TriggerTrait(name       = f'Set {target}',
                                     command    = target,
                                     key        = ky+'Trigger',
                                     actionKeys = [ky,calcWeather]))
            traits.append(SendtoTrait(mapName     = f'Board',
                                      boardName   = f'Board',
                                      restoreName = '',
                                      restoreKey  = '',
                                      name        = '',
                                      destination = 'R', #R for region
                                      region      = f'weather {target}',
                                      key         = ky))
            lk.append(ky)
            
        rt  = f'{{"! {vn} weather is "+{var}}}'

        traits.extend([
            ReportTrait(*lk,report = rt),
            RestrictCommandsTrait(
                name = 'Restrict to weather phase',
                hideOrDisable = 'Disable',
                expression = f'{{Phase!="{phaseNames[0]}"}}',
                keys = [t['key'] for t in trgs])]
                      +trgs) # Seems these must be after restrict
        # Seems this must be last
        traits.append(RestrictAccessTrait(sides=[], description='No move'))


# --------------------------------------------------------------------
def hideTraits(hideKey,
               hideIcon,
               obscuraIcon,
               faction,
               expression):
    # Restrict hide so that
    #
    #  Can only hide when setting up
    #  or if hidden already
    #
    #  First is !notSetup
    #  Second is ObscuredToOthers
    #
    # So enable with
    #
    #    ObscruredToOthers || !notSetup
    #
    # Of course, we need to invert this
    #
    #    !ObscuredToOthers && notSetup
    # v(f'Add hide to Argentine prototype')
    hideTxt  = '{ObscuredToOthers?"Reveal":"Hide"}'
    hide     = MaskTrait(keyCommand   = hideKey,
                         imageName    = hideIcon,
                         hideCommand  = r'Reveal\/Hide',
                         access       = [faction,'<observer>'],
                         displayStyle = MaskTrait.IMAGE+obscuraIcon,
                         peekCommand  = '')
    # v(f'Hide trait: {hide.encode()}')
    rt      = 'PieceName+" "+(ObscuredToOthers?"hidden":"revealed")'
    repHide = ReportTrait(hideKey, report = f'{{{rt}}}')
    resHide = RestrictCommandsTrait(name          = f'Restrict {hideIcon}',
                                    hideOrDisable = 'Disable',
                                    expression    = f'{{{expression}}}',
                                    keys          = [hideKey])
    return [resHide,hide,repHide]


# --------------------------------------------------------------------
def removeLayer(traits,*args):
    ftrait  = Trait.findTrait(traits,LayerTrait.ID,*args)
    if ftrait is not None:
        traits.remove(ftrait)

# --------------------------------------------------------------------
def removeFaction(traits,faction):
    ftrait  = Trait.findTrait(traits,PrototypeTrait.ID,
                              'name',f'{faction} prototype')
    if ftrait is not None:
        traits.remove(ftrait)
    
# --------------------------------------------------------------------
def afCounterTraits(traits,
                    main,
                    name,
                    faction,
                    facShort,
                    incrAFKey,
                    decrAFKey):
    planeStart = {'uk harrier avail':    [8,10],
                  'ag skyhawk avail':    [12,12],
                  'ag mirage iii avail': [14,14] }
    
    traits.append(PrototypeTrait(name='Markers prototype'))
    # Remove flip traits
    removeLayer(traits)

    # Plane name 
    plane   = name[3:].replace(' avail','')
    var     = plane.replace(' ','_') + 'AF'
    track   = facShort + ' AF '
    start   = track+str(planeStart[name][0])
    maxAF   = planeStart[name][1]
    atStart = main.addAtStart(name        = name,
                              location    = start,
                              owningBoard = 'Board')
    expr    = 'Integer.parseInt(LocationName.replaceAll("[^0-9]",""))'
    iCode   = f'{{"{track}"+Math.min({var}+1,{maxAF})}}'
    dCode   = f'{{"{track}"+Math.max({var}-1,0)}}'
    rt      = f'{{"{faction} {plane} AF: "+{var}}}'
    traits.extend([
        CalculatedTrait(name        = var,
                        expression  = expr,
                        description = 'Calculate current value'),
        SendtoTrait(mapName     = f'Board',
                    boardName   = f'Board',
                    restoreName = '',
                    restoreKey  = '',
                    name        = 'Increment',
                    destination = 'R', #R for region
                    region      = iCode,
                    key         = incrAFKey),
        SendtoTrait(mapName     = f'Board',
                    boardName   = f'Board',
                    restoreName = '',
                    restoreKey  = '',
                    name        = 'Decrement',
                    destination = 'R', #R for region
                    region      = dCode,
                    key         = decrAFKey),
        ReportTrait(incrAFKey,decrAFKey,  report = rt),
        RestrictAccessTrait(sides=[], description='Cannot move')])

# --------------------------------------------------------------------
def backToOOB(faction,name):
    oob         = f'{faction} OOB'
    return SendtoTrait(name        = 'Remove',
                       mapName     = oob,
                       boardName   = oob,
                       key         = key('E'),
                       restoreName = 'Restore',
                       restoreKey  = key('R'),
                       destination = 'R',
                       region      = name + '@' + faction + ' oob')

# --------------------------------------------------------------------
def beachTraits(beachKey,selectKey,cleanKey,notSetup):
    traits = [
        SendtoTrait(mapName     = f'Board',
                    boardName   = f'Board',
                    restoreName = '',
                    restoreKey  = '',
                    name        = '',
                    destination = 'R', #R for region
                    region      = 'uk lz 1',
                    key         = beachKey),
        TriggerTrait(name       = 'Select',
                     command    = 'Select',
                     key        = selectKey,
                     actionKeys = [beachKey,key('F')]),
        RestrictCommandsTrait(name          = 'Restrict',
                              hideOrDisable = 'Disable',
                              expression = f'{{{notSetup}}}',
                              keys = [selectKey,beachKey]),
        DeleteTrait(name = 'Clean', key=cleanKey),
        RestrictAccessTrait(sides=[],
                            description='No move')]
    return traits

    
# --------------------------------------------------------------------
# --------------------------------------------------------------------
# --------------------------------------------------------------------
# --------------------------------------------------------------------
# --------------------------------------------------------------------

# --------------------------------------------------------------------
# Our main patching function
#
# Message prefixes
#
#  | black
#  ! green
#  ? pink
#  ~ red
#  ` violet
#
def patch(build,data,vmod,verbose=False):
    from re  import sub

    Verbose().setVerbose(verbose)
    verb = VerboseGuard()

    # Names and keys 
    notSetup         = 'NotInitialSetup'
    primaryWeather   = 'primaryWeather'
    secondaryWeather = 'secondaryWeather'
    mpName           = 'MP'
    startKey         = key(NONE,0)+',startHex'
    beachKey         = key(NONE,0)+',beach'
    testInit         = key(NONE,0)+',testInitial' # key
    isInit           = key(NONE,0)+',isInitial'
    checkKey         = key(NONE,0)+',checkMove'
    elimKey          = key(NONE,0)+',eliminate'
    incrMPKey        = key(NONE,0)+',incrMP'
    decrMPKey        = key(NONE,0)+',decrMP'
    incrAFKey        = key('=',SHIFT)# +',incrAF'
    decrAFKey        = key('-',0)# +',decrAF'
    calcWeather      = key(NONE,0)+',calcWeather'
    resolveWeather   = key(NONE,0)+',resolveWeather'
    rollDice         = key(NONE,0)+',rollDice'
    unhideAFs        = key(NONE,0)+',unhideAFs'
    isolatedKey      = key('I')
    suppliedKey      = key('J')
    hideKey          = key('H')
    selectKey        = key('S')
    cleanKey         = key('D')
    
    game = build.getGame()
    gp   = game.getGlobalProperties()[0];
    gp.addProperty(name         = notSetup,
                   initialValue = False,
                   description  = 'Whether we are not at the start')
    gp.addProperty(name         = primaryWeather,
                   initialValue = 'Clear',
                   description  = 'Primary weather')
    gp.addProperty(name         = secondaryWeather,
                   initialValue = 'None',
                   description  = 'Secondary weather')
    game.addStartupMassKey(name       = 'Hide Argentine units',
                           hotkey     = hideKey,
                           buttonText   = '', 
                           icon         = '',
                           reportSingle = False,
                           reportFormat = 'Hiding all Argentine units',
                           singleMap    = False,
                           target       = '',
                           whenToApply  = StartupMassKey.START_GAME)
    gopts = game.getGlobalOptions()[0]
    gopts['nonOwnerUnmaskable'] = 'Always'
                       
    # ----------------------------------------------------------------
    # Extra documentation 
    doc  = game.getDocumentation()[0]
    doc.addHelpFile(title='More information',fileName='help/more.html')
    vmod.addFile('help/more.html', moreHelp.format(title=game['name']))

    # ----------------------------------------------------------------
    # Some fixes for main and dead map
    maps                     = game.getMaps()
    main                     = maps['Board']
    main['moveToFormat']     = '` '+ main['moveToFormat']
    main['moveWithinFormat'] = '` '+ main['moveWithinFormat']
    main['moveKey']          = checkKey
    dead                     = maps['DeadMap']
    dead['icon']             = 'pool-icon.png'
    restore                  = dead.getMassKeys()['Restore']
    restore['icon']          = 'restore-icon.png'

    # ----------------------------------------------------------------
    # Set custom icons on some global keys 
    mkeys = main.getMassKeys()
    mkeys['Eliminate']['icon'] = 'eliminate-icon.png'
    mkeys['Flip']     ['icon'] = 'flip-icon.png'

    oobs                       = game.getChartWindows()['OOBs']
    oobs['icon']               = 'oob-icon.png'
    
    # ----------------------------------------------------------------
    # Get Zoned area
    # Adjust the hexes
    zoned        = main.getBoardPicker()[0].\
        getBoards()['Board'].getZonedGrids()[0]
    zones        = zoned.getZones()
    hzone        = zones['hexes']
    hgrids       = hzone.getHexGrids()
    hgrid        = hgrids[0]
    hnum         = hgrid.getNumbering()[0]
    hnum['vOff'] = 1
    hnum['sep']  = '/'

    # ----------------------------------------------------------------
    turns          = game.getTurnTracks()['Turn']
    phaseNames     = ['Weather',
                      'AF assignment',
                      'Argentine movement',
                      'British amphibious',
                      'Argentine combat',
                      'British movement',
                      'British combat'
                      ]
    phases         = turns.getLists()['Phase']
    phases['list'] = ','.join(phaseNames)


    main.addMassKey(name         = 'Toggle isolated',
                    buttonHotkey = isolatedKey,
                    hotkey       = isolatedKey,
                    buttonText   = '', 
                    icon         = 'isolated-icon.png',
                    reportSingle = True,
                    reportFormat = '',
                    tooltip      = 'Mark isolated')
    main.addMassKey(name         = 'Mark supplied',
                    buttonHotkey = suppliedKey,
                    hotkey       = suppliedKey,
                    buttonText   = '', 
                    icon         = 'deisolated-icon.png',
                    reportSingle = True,
                    reportFormat = '',
                    tooltip      = 'Mark supplied')
    main.addMassKey(name         = 'Toggle hide',
                    buttonHotkey = hideKey,
                    hotkey       = hideKey,
                    buttonText   = '', # g['name']
                    icon         = 'hide-icon.png',
                    reportSingle = True,
                    reportFormat = '')
    main.addMassKey(name         = 'Increase Argentine MPs',
                    buttonHotkey = incrMPKey,
                    hotkey       = incrMPKey,
                    buttonText   = '', 
                    icon         = 'mp++.png',
                    target       = '',
                    reportSingle = True,
                    reportFormat = '',
                    filter       = '{BasicName == "morale"}')
    main.addMassKey(name         = 'Decrease Argentine MPs',
                    buttonHotkey = decrMPKey,
                    hotkey       = decrMPKey,
                    buttonText   = '', 
                    icon         = 'mp--.png',
                    target       = '',
                    reportSingle = True,
                    reportFormat = '',
                    filter       = '{BasicName == "morale"}')
    main.addMassKey(name         = 'Increase MPs or AFs',
                    buttonHotkey = incrAFKey,
                    hotkey       = incrAFKey,
                    buttonText   = '', 
                    icon         = '',
                    reportSingle = True,
                    reportFormat = '')
    main.addMassKey(name         = 'Decrease MPs or AFs',
                    buttonHotkey = decrAFKey,
                    hotkey       = decrAFKey,
                    buttonText   = '', 
                    icon         = '',
                    reportSingle = True,
                    reportFormat = '')
    main.addMassKey(name         = 'Load Argentine setup',
                    icon         = 'ag-icon.png',
                    buttonHotkey = key('A',CTRL_SHIFT),
                    hotkey       = startKey,
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    canDisable   = True,
                    propertyGate = notSetup,
                    reportFormat = '! <b>Load Argentine setup</b>',
                    filter       = '{Faction=="Argentine"}')
    main.addMassKey(name         = 'Load British setup',
                    icon         = 'uk-icon.png',
                    buttonHotkey = key('B',CTRL_SHIFT),
                    hotkey       = startKey,
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    canDisable   = True,
                    propertyGate = notSetup,
                    reportFormat = '! <b>Load British setup</b>',
                    filter       = '{Faction=="British"}')
    main.addMassKey(name         = 'Test for initial setup',
                    icon         = '',
                    buttonHotkey = testInit,
                    hotkey       = isInit,
                    buttonText   = '',
                    target       = '',
                    filter       = '{BasicName == "game turn"}')
    main.addMassKey(name         = 'Un hide AFs',
                    reportFormat = '{"Unhiding assigned AFs"}',
                    buttonHotkey = unhideAFs,
                    hotkey       = hideKey,
                    buttonText   = '',
                    target       = '',
                    singleMap    = False,
                    filter       = ('{AirHidden}'))
    turns.addHotkey(hotkey       = testInit,
                    match        = '{true}',
                    name         = 'Test initial turn')
    turns.addHotkey(hotkey       = unhideAFs,
                    match        = f'{{Phase=="{phaseNames[2]}"}}',
                    name         = 'Unhide AFs',
                    reportFormat = '{"Revealing assigned AFs"}')
    turns.addHotkey(name  = 'End of game',
                    hotkey = '',
                    match = '{Turn>15}',
                    reportFormat = '{Alert("End of game. Tally VPs")}')

    # ----------------------------------------------------------------
    # Keys on OOBs
    #
    agoob = maps['Argentine OOB']
    ukoob = maps['British OOB']
    ukoob.addMassKey(name         = 'Select LZ',
                     buttonHotkey = selectKey,
                     hotkey       = selectKey,
                     buttonText   = 'Select LZ', # g['name']
                     reportSingle = True,
                     reportFormat = '')
    ukoob.addMassKey(name         = 'Clean-up LZs',
                     buttonHotkey = cleanKey,
                     hotkey       = cleanKey,
                     buttonText   = 'Clean-up LZs', # g['name']
                     reportSingle = False,
                     reportFormat = '',
                     target       = '',
                     filter       = '{BasicName.contains("uk beach")}')
                     

    # ----------------------------------------------------------------
    # Container of prototypes
    prototypeContainer = game.getPrototypes()[0]
    prototypes         = prototypeContainer.getPrototypes()
    
    # ----------------------------------------------------------------
    # Define global keys for turn marker
    #
    turnPrototypes = makeTurnPrototypes(main,
                                        turns,
                                        phaseNames,
                                        prototypeContainer)

    # ----------------------------------------------------------------
    makeMPPrototypes(incrMPKey,
                     decrMPKey,
                     incrAFKey,
                     decrAFKey,
                     prototypeContainer)
    
    # ----------------------------------------------------------------
    # Isolated trait
    isolated = LayerTrait(['',
                           'isolated-mark.png'],
                          ['','Isolated +'],
                          activateName = '',
                          activateMask = '',
                          activateChar = '',
                          increaseName = 'Isolated',
                          increaseMask = CTRL,
                          increaseChar = 'I',
                          decreaseName = '',
                          decreaseMask = '',
                          decreaseChar = '',
                          resetName    = 'Supplied',
                          resetKey     = suppliedKey,
                          under        = False,
                          underXoff    = 0,
                          underYoff    = 0,
                          loop         = False,
                          name         = 'Isolated',
                          description  = '(Un)Mark unit as isolated',
                          always       = False,
                          activateKey  = '',
                          increaseKey  = isolatedKey,
                          decreaseKey  = '',
                          scale        = 1)
    repIso  = ReportTrait(key('F'),isolatedKey,suppliedKey,
                          report = ('{{BasicName+" isolation level: '
                                    '"+Isolated_Level}}'))
    
    # ----------------------------------------------------------------
    # Now modify the faction prototypes
    factionElim = {}
    with VerboseGuard('Fixing up faction prototypes') as v:
        for faction in ['Argentine', 'British']:
            prototype = prototypes[f'{faction} prototype']
            traits    = prototype.getTraits()
            basic     = traits.pop()
            facShort  = {'Argentine': 'ag',
                         'British':   'uk'}[faction]
            
            dtrait  = Trait.findTrait(traits,DeleteTrait.ID)
            if dtrait is not None:
                traits.remove(dtrait)
            rtrait  = Trait.findTrait(traits,RotateTrait.ID)
            if rtrait is not None:
                rtrait['nangles'] = 12
        
            etrait  = Trait.findTrait(traits,SendtoTrait.ID,
                                      'name', 'Eliminate')
            factionElim[faction] = etrait
            
            prototypeContainer.addPrototype(
                name        = f'{faction} support',
                traits      = traits+[BasicTrait()],
                description = f'{faction} support units')
        
        
            if faction == 'Argentine':
                traits.extend(hideTraits(hideKey,
                                         'ag_hide_1.png',
                                         'obscura.png',
                                         'Argentine',
                                         f'!ObscuredToOthers && {notSetup}'))
                v(f'Added hide prototype to {faction} prototype')
            
            traits.extend([isolated, repIso])
        
            prototype.setTraits(*traits,basic)
        
            prototypeContainer.addPrototype(
                name        = f'{faction} markers',
                traits      = [MarkTrait('Faction',faction),
                               MarkTrait('Air_Factors',True),
                               CalculatedTrait(
                                   name="AirHidden",
                                   expression="{ObscuredToOthers}",
                                   description="Air unit hidden"),
                               rtrait,
                               BasicTrait()],
                description = f'{faction} markers')

            atraits = [MarkTrait('Faction',faction),
                       rtrait]
            expr  = f'Phase!="{phaseNames[1]}"&&Phase!="{phaseNames[2]}"'
            expr2 = ('!ObscuredToOthers && ('+
                     '||'.join([f'LocationName=="{b}"'
                                for b in ['uk tas',
                                          'uk cap',
                                          'uk airfield suppression',
                                          'ag nas',
                                          'ag tas']])+')')
            atraits.extend(hideTraits(hideKey,
                                      f'{facShort}-hide-plane.png',
                                      'afobscura.png',
                                      faction,
                                      expr))
            atraits.extend([
                TriggerTrait(name       = 'Hide AF in boxes',
                             command    = '',
                             key        = checkKey,
                             actionKeys = [hideKey],
                             property   = f'{{{expr2}}}'),
                BasicTrait()])
            prototypeContainer.addPrototype(
                name        = f'{faction} AF',
                traits      = atraits,
                description = f'{faction} AFs')
            
    # ----------------------------------------------------------------
    # Clean markers prototype
    prototype = prototypes['Markers prototype']
    traits    = prototype.getTraits()
    basic     = traits.pop()
    prototype.setTraits(basic)
    
    # ----------------------------------------------------------------
    # Get all pieces and add Eliminate action that moves the unit to
    # the OOB. This is also where we add the traits to move the game
    # turn marker on the turn track.
    with VerboseGuard('Fixing up pieces') as v:
        pieces     = game.getPieces(asdict=False)
        factionMap = {'uk': 'British',
                      'ag': 'Argentine'}
        
        for num, piece in enumerate(pieces):
            name     = piece['entryName'].strip()
            facShort = name[:2]
            faction  = factionMap.get(facShort,'Markers')
            traits   = piece.getTraits()
            basic    = traits.pop()
            facTrait = Trait.findTrait(traits,PrototypeTrait.ID,
                                       'name',f'{faction} prototype')
            atStart  = None
            v(f'- {name} (faction={faction})')
            
            if faction in list(factionMap.values()):
                hx = revHexes.get(name,None)

                if hx is not None:
                    # v(f'Set start hex {hx} for {name}')
                    pl = hx[:2]+r'\/'+hx[2:] if len(hx) == 4 else hx
                    startup = SendtoTrait(name        = '',
                                          key         = startKey,
                                          mapName     = 'Board',
                                          boardName   = 'Board',
                                          destination = 'G',
                                          restoreName = '',
                                          restoreKey  = '',
                                          # position    = hx
                                          position    = pl
                                          )
                    rt     = '!  $newPieceName$ moved to start hex $location$'
                    report = ReportTrait(startKey,report = rt)
                    traits.extend([startup,report])

                # Remove faction prototype and replace with faction markers
                if any([mark in name
                        for mark in ['airfield',
                                     'improved',
                                     'supply',
                                     'hide',
                                     'beach',
                                     'naval group']]):
                    if facTrait is not None:
                        traits.remove(facTrait)

                    if 'beach' not in name:
                        traits.insert(0,
                                      PrototypeTrait(name=f'{faction} markers'))

                    if 'hide' in name:
                        traits.append(factionElim[faction])

                # Remove faction prototype and replace with faction support
                if any([mark in name
                        for mark in ['helicopter',
                                     'apc',
                                     'glamorgan',
                                     'brilliant',
                                     'arrow',
                                     'plymouth',
                                     'intrepid',
                                     'fearless',
                                     'lancelot',
                                     'galahad',
                                     'bedivere',
                                     'antrim',
                                     'broadsword',
                                     'ardent']]):
                    if facTrait is not None:
                        traits.remove(facTrait)

                    if not name.endswith('avail'):
                        traits.insert(0,
                                      PrototypeTrait(name=f'{faction} support'))

                # Add an eliminate key that sends back to OOB
                if any([mark in name
                        for mark in ['airfield',
                                     'improved',
                                     'supply',
                                     'destroyed bridge']]):
                    traits.append(backToOOB(faction,name))

                # Move naval group marker to slot
                if 'naval group' in name:
                    atStart = main.addAtStart(name      = name,
                                              location  = ('uk naval '
                                                           + name[-1].upper()),
                                              owningBoard = 'Board')

                elif name.endswith('avail'):
                    if facTrait is not None:
                        traits.remove(facTrait)
                    afCounterTraits(traits,
                                    main,
                                    name,
                                    faction,
                                    facShort,
                                    incrAFKey,
                                    decrAFKey)
                elif any([mark in name
                          for mark in ['mirage iii',
                                       'skyhawk',
                                       'pucara',
                                       'harrier']]):
                    if facTrait is not None:
                        traits.remove(facTrait)
                    traits.insert(0,PrototypeTrait(name=f'{faction} AF'))
                    traits.append(backToOOB(faction,name))
                    
                if 'beach' in name:
                    traits.extend(beachTraits(beachKey,
                                              selectKey,
                                              cleanKey,
                                              notSetup))
            else: # Not faction specific
                if name == 'game turn':
                    removeFaction(traits,'British')
                    removeLayer(traits)

                    traits.extend([PrototypeTrait(pnn + ' prototype')
                                   for pnn in turnPrototypes])
                    traits.append(GlobalPropertyTrait(
                        ['',isInit,GlobalPropertyTrait.DIRECT,
                         f'{{Turn!=1||Phase!="Weather"}}'
                         ],
                        name = notSetup))
                    traits.append(RestrictAccessTrait(sides=[],
                                                      description='No move'))

                elif name == 'morale':
                    removeFaction(traits,'Argentine')
                    removeLayer(traits)

                    traits.extend([PrototypeTrait('Argentine MP')])
                    v(traits)
                    atStart = main.addAtStart(name        = name,
                                              location    = 'MP 12',
                                              owningBoard = 'Board')
                    
                elif 'weather' in name:
                    weatherTraits(name,
                                  traits,
                                  main,
                                  phaseNames,
                                  primaryWeather,
                                  secondaryWeather,
                                  rollDice,
                                  calcWeather,
                                  resolveWeather)

            # Set the massaged traits
            piece.setTraits(*traits,basic)
            if atStart is not None:
                atStart.addPieces(piece)                
                    

    # from pprint import pprint
    # pprint(revHexes)
#
# EOF
#
