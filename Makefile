#
#
#
NAME		:= PortStanley
VERSION		:= 1.0
VMOD_VERSION	:= 1.0.0
LATEX		:= lualatex
LATEX_FLAGS	:= -interaction=nonstopmode	\
		   -file-line-error		\
		   -synctex 15			\
		   -shell-escape
EXPORT          := $(shell kpsewhich wgexport.py)
EXPORT_FLAGS	:=
PDFTOCAIRO	:= pdftocairo
CAIROFLAGS	:= -scale-to 800
PDFJAM		:= pdfjam
REDIR		:= > /dev/null 2>&1 
DATAFILES	:= materials.tex		\
		   tables.tex			\
		   units.tex			
DOCFILES	:= $(NAME).tex			\
		   front.tex			\
		   preface.tex			\
		   rules.tex			\
		   $(DATAFILES)
OTHER		:= board.tex			\
		   calcsplit.tex		\
		   spine.tex
STYFILES	:= ps.sty			\
		   commonwg.sty
BOARD		:= board.pdf
SIGNATURE	:= 44
PAPER		:= --a4paper
TARGETS		:= $(NAME).pdf 			\
		   $(NAME)Booklet.pdf 		\
		   splitboardA4.pdf		\
		   materials.pdf		\
		   board.pdf

TARGETSA4	:= $(NAME)A4.pdf 		\
		   $(NAME)A4Booklet.pdf 	\
		   tablesA4.pdf			\
		   tablesA4Booklet.pdf		\
		   splitboardA3.pdf		\
		   splitboardA4.pdf		\
		   materialsA4.pdf		\
		   board.pdf			\
		   $(NAME).vmod

TARGETSLETTER	:= $(NAME)Letter.pdf 		\
		   $(NAME)LetterBooklet.pdf 	\
		   splitboardTabloid.pdf	\
		   splitboardLetter.pdf		\
		   materialsLetter.pdf

ifdef VERBOSE	
MUTE		:=
REDIR		:=
LATEX_FLAGS	:= -synctex 15	-shell-escape
EXPORT_FLAGS	:= -V
else
MUTE		:= @
REDIR		:= > /dev/null 2>&1 
endif

calcsplit%.pdf splitboard%.tex:calcsplit.tex $(BOARD) $(STYFILES) $(BOARD)
	@echo "LATEX calcsplit ($* - explicit)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname calcsplit$* $< $(REDIR)

splitboard%.pdf:splitboard%.tex
	@echo "LATEX splitboard$* ($* - explicit)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $< $(REDIR)

%.pdf:	%.tex
	@echo "LATEX	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%.aux:	%.tex
	@echo "LATEX(1)	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%.pdf:	%.aux
	@echo "LATEX(2)	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%A4.pdf:%.tex
	@echo "LATEX	$* (A4)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A4.aux:%.tex
	@echo "LATEX(1)	$*A4"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A4.pdf:%A4.aux
	@echo "LATEX(2)	$*A4"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A3.aux:%.tex
	@echo "LATEX(1)	$*A3"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A3 $* $(REDIR)

%A3.pdf:%A3.aux
	@echo "LATEX(2)	$*A3"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A3 $* $(REDIR)

%A3.pdf:%.tex
	@echo "LATEX	$* (A3)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A3 $* $(REDIR)


%Letter.aux:	%.tex
	@echo "LATEX(1)	$*Letter"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Letter.pdf:	%Letter.aux
	@echo "LATEX(2)	$*Letter"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Letter.pdf:	%.tex
	@echo "LATEX	$* (Letter)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Tabloid.aux:	%.tex
	@echo "LATEX(1)	$*Tabloid"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Tabloid $* $(REDIR)

%Tabloid.pdf:	%Tabloid.aux
	@echo "LATEX(2)	$*Tabloid"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Tabloid $* $(REDIR)

%Tabloid.pdf:	%.tex
	@echo "LATEX	$* (Tabloid)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Tabloid $* $(REDIR)


%Booklet.pdf:%.pdf
	@echo "BOOKLET	$*"
	$(MUTE)pdfjam 				\
		--landscape 			\
		--suffix book 			\
		--signature '$(SIGNATURE)' 	\
		$(PAPER) 			\
		--no-tidy 			\
		--outfile $@ 			\
		-- $< '1-$(SIGNATURE)' $(REDIR)


%A3Booklet.pdf:%A4.pdf
	@echo "BOOKLET	$* (A3)"
	$(MUTE)pdfjam 				\
		--landscape 			\
		--signature '$(SIGNATURE)' 	\
		--a3paper 			\
		--no-tidy 			\
		--outfile $@ 			\
		-- $< '1-$(SIGNATURE)' $(REDIR)




%.png:%.pdf
	$(PDFTOCAIRO) $(CAIROFLAGS) -singlefile -png $< $*

%.png:%A4.pdf
	$(PDFTOCAIRO) $(CAIROFLAGS) -singlefile -png $< $*

all:	$(TARGETS)
a4:	$(TARGETSA4)
us:	$(TARGETSLETTER)
vmod:	$(NAME).vmod
letter:	us
mine:	a4 spine.pdf logo.png frontA4.pdf 

cache:
	$(MUTE)mkdir -p cache
	$(MUTE)if test ! -f cache/.gitignore ; then touch cache/.gitignore; fi

distdir:$(TARGETS) README.md
	@echo "DISTDIR	$(NAME)-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-$(VERSION)
	$(MUTE)cp $^ $(NAME)-$(VERSION)/
	$(MUTE)touch $(NAME)-$(VERSION)/*

dist:	distdir
	@echo "DIST	$(NAME)-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-$(VERSION).zip $(NAME)-$(VERSION)
	$(MUTE)rm -rf $(NAME)-$(VERSION)

distdirA4:$(TARGETSA4) README.md
	@echo "DISTDIR	$(NAME)-A4-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-A4-$(VERSION)
	$(MUTE)cp $^ $(NAME)-A4-$(VERSION)/
	$(MUTE)touch $(NAME)-A4-$(VERSION)/*

distA4:	distdirA4
	@echo "DIST	$(NAME)-A4-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-A4-$(VERSION).zip $(NAME)-A4-$(VERSION)
	$(MUTE)rm -rf $(NAME)-A4-$(VERSION)

distdirLetter:$(TARGETSLETTER) README.md
	@echo "DISTDIR	$(NAME)-Letter-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-Letter-$(VERSION)
	$(MUTE)cp $^ $(NAME)-Letter-$(VERSION)/
	$(MUTE)touch $(NAME)-Letter-$(VERSION)/*

distLetter:	distdirLetter
	@echo "DIST	$(NAME)-Letter-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-Letter-$(VERSION).zip $(NAME)-Letter-$(VERSION)
	$(MUTE)rm -rf $(NAME)-Letter-$(VERSION)


clean:
	@echo "CLEAN"
	$(MUTE)rm -f *~ *.log *.aux *.out *.lot *.lof *.toc *.auxlock
	$(MUTE)rm -f *.synctex* *.pdf *-pdfjam.pdf *.vmod *.json
	$(MUTE)rm -f board.png board.svg logo.png tables.png
	$(MUTE)rm -f tables.png oob.png counters.png front.png 
	$(MUTE)rm -f $(TARGETS) $(TARGETSA4) $(TARGETSLETTER)
	$(MUTE)rm -f .oldschool calcsplit*_out.tex
	$(MUTE)rm -f wargame.zip splitboard*.tex
	$(MUTE)rm -rf oldschool*

realclean: clean
	@echo "CLEAN ALL"
	$(MUTE)rm -rf cache/*  labels*.tex
	$(MUTE)rm -rf $(NAME)-*-$(VERSION)
	$(MUTE)rm -rf __pycache__ oldschool*

distclean:realclean
	@echo "CLEAN DIST"
	$(MUTE)rm -rf *.zip
	$(MUTE)rm -rf $(NAME)-$(VERSION)
	$(MUTE)rm -rf $(NAME)-Letter-$(VERSION)
	$(MUTE)rm -rf $(NAME)-A4-$(VERSION)

$(NAME).pdf:		$(NAME).aux
$(NAME).aux:		$(DOCFILES) 	$(STYFILES) 	splitboardA4.pdf
materials.pdf:		$(DATAFILES)	$(STYFILES)	splitboardA4.pdf
board.pdf:		board.tex	$(STYFILES)

$(NAME)A4.pdf:		$(NAME)A4.aux
$(NAME)A4.aux:		$(DOCFILES) 	$(STYFILES)     splitboardA4.pdf
materialsA4.pdf:	$(DATAFILES)	$(STYFILES)	splitboardA4.pdf
tablesA4.pdf:		tables.tex      $(DATAFILES)    $(STYFILES)
frontA4.pdf:		front.tex       $(DATAFILES)    $(STYFILES)
tablesA3Booklet.pdf:	tablesA4.pdf
tablesA3Booklet.pdf:	SIGNATURE:=4

$(NAME)Letter.pdf:	$(NAME)Letter.aux
$(NAME)Letter.aux:	$(DOCFILES) 	$(STYFILES) 	splitboardLetter.pdf
materialsLetter.pdf:	$(DATAFILES)	$(STYFILES)	splitboardLetter.pdf
tablesLetter.pdf:	tables.tex      $(DATAFILES)    $(STYFILES)

frontA4.pdf:		front.tex       board.pdf       $(STYFILES)
$(NAME)LetterBooklet.pdf:PAPER=--letterpaper
$(NAME)LetterBooklet.pdf:SIGNATURE=48
tablesA4Booklet.pdf:	 SIGNATURE=8
tablesLetterBooklet.pdf: SIGNATURE=8 
tablesLetterBooklet.pdf: PAPER=--letterpaper

splitboardA3.pdf:	splitboardA3.tex
splitboardA4.pdf:	splitboardA4.tex
splitboardLetter.pdf:	splitboardLetter.tex
splitboardTabloid.pdf:	splitboardTabloid.tex

logo.png:		CAIROFLAGS:=-scale-to 100

export.pdf:	export.tex tables.tex units.tex board.tex $(STYFILES)
rules.pdf:$(NAME)A4.pdf
	@echo "PDFJAM	$@ ($<)"
	$(MUTE)$(PDFJAM) \
		--outfile $@ 			\
		-- $< '1-$(SIGNATURE)' $(REDIR)

# Temporary patch.py rules.pdf
$(NAME).vmod:export.pdf patch.py rules.pdf 
	@echo "VMOD	$(NAME).vmod"
	$(MUTE)$(EXPORT) export.pdf export.json -p patch.py \
		-v $(VMOD_VERSION) -t "Port Stanley" -r rules.pdf \
		-d "This module was created from LaTeX sources" \
		-o $(NAME).vmod -N -C $(EXPORT_FLAGS) $(REDIR)

docker-prep:
	apt update
	apt install -y wget python3-pil poppler-utils 
	wget "https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/download?job=dist" -O wargame.zip
	mkdir -p ${HOME}/texmf
	unzip -o wargame.zip -d ${HOME}/texmf

docker-artifacts: distdirA4 distdirLetter 

#
# EOF
#

